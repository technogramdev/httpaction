const _ = require("underscore");
const utility    = {};

/**
 * Check for Empty value
 * @return boolean (true|false)
 */
utility.isEmpty = function (val) {
    if(typeof val  == "number"){
        return val == "" ? true : false
    }
    return _.isEmpty(val)
};

/**
 * check string is valid or return default value
 * @param {string} value
 * @param {string} defaultValue
 *
 * @return string
 */
utility.validOrDefault = function (value, defaultValue = '') {
    if (utility.isEmpty(value) === false) {
        return (_.isString(value) === true) ? value : defaultValue;
    }
    return defaultValue;
};

/**
 * Check for value Exits in Array or not 
 * @param Array of value  and  element to check 
 * @return boolean (true|false)
 */
utility.indexOf = function (arr, el) {
    if (utility.isEmpty(arr) === true) {
        return false;
    } else if( _.indexOf(arr,el) > -1) {
        return true;
    }
};


/**
 * set cookie
 * @param {string} name,value
 * @param {number} days
 */ 

utility.setCookie = function (name, value, days, domain="") {
    let expires = "";
    if (days) {
        let date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toGMTString();
    }
        let primaryDomain = "; domain=" + domain;
    
    document.cookie   = name + "=" + value + expires + primaryDomain + "; path=/";
};

/**
 * remove cookie
 * @param {string} name
 */ 

utility.removeCookie = function (name) {
    utility.setCookie(name, "", -1);
};

/**
 * get cookie
 * @param {string} name
 * @return {string} value
 */ 


utility.getCookie = function (name) {
    let nameEQ = name + "=";
    let ca     = document.cookie.split(';');
    for (let i = 0; i < ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1, c.length);
        }
        if (c.indexOf(nameEQ) == 0) {
            return c.substring(nameEQ.length, c.length);
        }
    }
    return null;
};

/*
*get url params
*@params {JSON} jsonobject
*@return {string} query string
*/

utility.jsonToQueryString=function (json) {
    console.log("json >>>>>>>>>>>> ",json);
return     
    return '?' + 
        Object.keys(json).map(function(key) {
            return encodeURIComponent(key) + '=' +
                encodeURIComponent(json[key]);
        }).join('&');
}


module.exports = {utility}