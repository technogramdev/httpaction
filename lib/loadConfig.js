const fs    = require("fs");
const path  = require("path");
var waterfall = require("async/waterfall");
const {utility}  = require("./util")
const dir = path.join(process.cwd(),"../config");

const env = process.env.NODE_ENV || "development";
const fileName = `config.${env}.json`;



/**
 * 
 * @description Get config 
 * @return {json}
 */
exports.loadConfig = function (cb) {

    const checkConfigDir = function (checkConfigDirCallback) {
        fs.readdir(dir,function(err,list){
            if(err){
                return checkConfigDirCallback(err);
            }
            return checkConfigDirCallback(null, list);
        });
    };

    const checkConfigFile = function (response,checkConfigFileCallback) {
        if(utility.indexOf(response,fileName)){
            return checkConfigFileCallback(null,path.join(dir,fileName))
        } else {
            return checkConfigFileCallback('No config file found in folder.');
        }
    };

    const getConfigData = function (response,getConfigDataCallback) {
        fs.readFile(response,'utf8', function(err, data) {
            if (err || utility.isEmpty(data)) {
                return getConfigDataCallback(err || "Couldn't found data in file!");
            }
            return getConfigDataCallback(null,data);
        });
    };

    waterfall([
        checkConfigDir,
        checkConfigFile,
        getConfigData
    ], cb);
}

