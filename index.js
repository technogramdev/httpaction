const request   = require('axios');
const uuid     = require("uuid/v4")();
const waterfall = require("async/waterfall");
const {loadConfig} = require("./lib/loadConfig");
const {utility}  = require("./lib/util")


exports.httpRequest = function (configName,params = {}) {

    return new Promise((resolve, reject) => {
        if(utility.isEmpty(configName)== true){
            reject("Configuration name is not valid!");
        }
        const configLoader = function (configLoaderCallback) {
            loadConfig(function (error, response) {
                if (error) {
                    return configLoaderCallback(error)
                }
                let res = JSON.parse(response)
                return configLoaderCallback(null,getApiConfig(res,configName));
            });
        };
    
        const callApi = function (serverConfig,callApiCallback) {
            httpRequestCall(serverConfig,params,function (error, response) {
                if (error) {
                    return callApiCallback(error)
                }
                return callApiCallback(null,response);
            });
        };
    
        waterfall([
            configLoader,
            callApi
        ], function(err,res){
            if(err){
                reject(err)
            } else {
                resolve(res.body)
            }
        });
    });
};



exports.httpRequestWithRetry = function (configName,params = {}) {

    return new Promise((resolve, reject) => {
        if(utility.isEmpty(configName)== true){
            reject("Configuration name is not valid!");
        }
        const configLoader = function (configLoaderCallback) {
            loadConfig(function (error, response) {
                if (error) {
                    return configLoaderCallback(error)
                }
                let res = JSON.parse(response)
                return configLoaderCallback(null,getApiConfig(res,configName));
            });
        };
    
        const callApi = function (serverConfig,callApiCallback) {
            httpRequestCall(serverConfig,params,function (error, response) {
                if (error) {
                    return callApiCallback(error)
                }
                return callApiCallback(null,response);
            });
        };
    
        waterfall([
            configLoader,
            callApi
        ], function(err,res){
            if(err){
                params.retry = params.retry || 1;
                if ( params.retry < 3 ) {
                    params.retry++;
                    params.timeout = (getApiConfig(res,configName)["timeout"]);
                    return this.httpRequestWithRetry(configName,params);
                } else {
                    reject(err);
                }
            } else {
                resolve(res.body)
            }
        });
    });
};



const getApiConfig  = function (c,rs) {
    try {
        let res = {};
        let cg = c[rs];
        let {isRelativeUrl = false} = c;
        let {timeout=6000,method="GET",url,server} = cg;
        res["timeout"] = timeout;
        res["method"] = method;
        let sg = c["serverConfig"][server];
        const { protocol, baseUrl,port=""} = sg;
        const p = port !== "" ? `:${port}` : ``;
        const u = isRelativeUrl ? `` : `${protocol}://${baseUrl}${p}${url}`;
        res["url"] =u;
        return res;
    } catch (error) {
        return res;
    }
    
};

const httpRequestCall  = function (serverConfig,params = {}, callback) {
    try {
        
        if (utility.isEmpty(serverConfig) == true) {
            return callback('[httpaction] : Server config is not valid!');
        }
        defaultHeaders = {
            "Content-Type": "application/json; charset=utf-8",
            "mode"        : 'no-cors'

        };
        let headers = Object.assign(defaultHeaders,params.headers);
        let method = params.method || serverConfig.method;
        let options = {};
        let qs = "";
        if(utility.isEmpty(params.payload) == false){
            qs = utility.jsonToQueryString(params.payload)
        }
        
        if(method == "GET"){
            options = {
                url    : serverConfig.url+qs,
                method : method,
                headers: headers,
                timeout: serverConfig.timeout
            };
        } else {
            options = {
                url    : serverConfig.url,
                method : method,
                headers: headers,
                data   : params.payload,
                timeout: serverConfig.timeout
            };
        }
        request(options)
            .then(function (response) {
                if(params.auth == true){
                    utility.removeCookie("uuid",uuid,1);
                    utility.setCookie("uuid",uuid,1);
                }
                response.body = response.data;
                delete response.data;
                return callback(null, response, response.body);
            })
            .catch(function (error) {
                return callback(error);
            });
    } catch (error) {
        return callback(error);
    }
};

